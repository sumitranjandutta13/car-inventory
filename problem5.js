module.exports = (year) =>{
    if(year === undefined || year.length === 0){
        return [];
    }else{
        let res = []
        for(i =0; i < year.length; i++){
            if(year[i] < 2000){
                res.push(year[i]);
            }
        }
        return res;
    }
}