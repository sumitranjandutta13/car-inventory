module.exports = (inventory) =>{
    if(inventory === undefined || inventory === 0){
        return [];
    }else{
        let res = [];
        for(i= 0; i< inventory.length; i++){
            res.push(inventory[i].car_year);
        }
        return res;
    }
}