module.exports = function(inventory){
    if(inventory === undefined || inventory.length === 0){
        return [];
    }else{
        return inventory[inventory.length - 1];
    }
}